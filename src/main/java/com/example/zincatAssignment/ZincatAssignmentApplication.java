package com.example.zincatAssignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZincatAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZincatAssignmentApplication.class);
		System.out.println("Application Up and Running......!!");
	}
}

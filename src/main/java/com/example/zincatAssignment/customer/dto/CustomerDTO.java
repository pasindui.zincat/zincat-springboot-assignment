package com.example.zincatAssignment.customer.dto;

import com.example.zincatAssignment.contactPerson.dto.ContactPersonDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO {

    private  Integer customerId;

    private int code;

    private String refrenceNo;

    private String customerType;

    private String customerName;

    private String companyName;

    private String nic;

    private String billingAddress;

    private String customerMobile;

    private String email;

    private String customerCountry;

    private String city;

    private String gender;

    private List<ContactPersonDTO> contactPersonDTOList;
}

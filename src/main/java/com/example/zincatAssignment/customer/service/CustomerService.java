package com.example.zincatAssignment.customer.service;

import com.example.zincatAssignment.common.entity.Customer;
import com.example.zincatAssignment.customer.dto.CustomerDTO;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * interface for customer service
 * @author pasindu
 */

@Component
public interface CustomerService {

    CustomerDTO findCustomerByCustomerName(String customerName);

    String saveCustomer (CustomerDTO customer);

    List<CustomerDTO> getAllCustomers();

    String updateCustomer (Customer customer);

    CustomerDTO customerDtoModelMapper(Customer customer);

    Customer customerEntityModelMapper(CustomerDTO customer);

}

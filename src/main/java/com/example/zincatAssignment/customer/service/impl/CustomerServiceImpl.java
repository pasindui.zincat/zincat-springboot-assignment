package com.example.zincatAssignment.customer.service.impl;

import com.example.zincatAssignment.common.entity.ContactPerson;
import com.example.zincatAssignment.common.entity.Customer;
import com.example.zincatAssignment.common.repository.CustomerRepository;
import com.example.zincatAssignment.contactPerson.dto.ContactPersonDTO;
import com.example.zincatAssignment.customer.dto.CustomerDTO;
import com.example.zincatAssignment.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Customer service class
 * @author pasindu
 */

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    /**
     * view individual customer by name
     * @param customerName
     * @return
     */
    @Override
    public CustomerDTO findCustomerByCustomerName(String customerName) {
        Customer customer = customerRepository.getCustomerByCustomerName(customerName);
        CustomerDTO customerDTO = customerDtoModelMapper(customer);
        return customerDTO;
    }

    /**
     * create a new customer
     * @param customer
     * @return
     */
    @Override
    public String saveCustomer(CustomerDTO customer) {
        System.out.println("step 2");
        Customer customer1 = customerEntityModelMapper(customer);
        customerRepository.save(customer1);
        return "customer saved!";
    }

    /**
     * get all the customers
     * @return
     */
    @Override
    public List<CustomerDTO> getAllCustomers() {
        List<Customer> customerList = customerRepository.findAll();
        List<CustomerDTO> customerDTOList = new ArrayList<>();
        return customerDTOList;
    }

    @Override
    public String updateCustomer(Customer customer) {
        return "customer updated!";
    }

    /**
     * customer DTO to customer entity modal mapper
     * @param customer
     * @return
     */

    @Override
    public CustomerDTO customerDtoModelMapper(Customer customer) {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setCustomerId(customer.getCustomerId());
        customerDTO.setCode(customer.getCode());
        customerDTO.setRefrenceNo(customer.getRefrenceNo());
        customerDTO.setCustomerType(customer.getCustomerType());
        customerDTO.setCustomerName(customer.getCustomerName());
        customerDTO.setCompanyName(customer.getCompanyName());
        customerDTO.setNic(customer.getNic());
        customerDTO.setBillingAddress(customer.getBillingAddress());
        customerDTO.setCustomerMobile(customer.getCustomerMobile());
        customerDTO.setEmail(customer.getEmail());
        customerDTO.setCustomerCountry(customer.getCustomerCountry());
        customerDTO.setCity(customer.getCity());
        customerDTO.setGender(customer.getGender());
        return customerDTO;
    }


    @Override
    public Customer customerEntityModelMapper(CustomerDTO customerDTO) {
        Customer customer1 = new Customer();
        customer1.setCode(customerDTO.getCode());
        customer1.setRefrenceNo(customerDTO.getRefrenceNo());
        customer1.setCustomerType(customerDTO.getRefrenceNo());
        customer1.setCustomerName(customerDTO.getCustomerName());
        customer1.setCompanyName(customerDTO.getCompanyName());
        customer1.setNic(customerDTO.getNic());
        customer1.setBillingAddress(customerDTO.getBillingAddress());
        customer1.setCustomerMobile(customerDTO.getCustomerMobile());
        customer1.setEmail(customerDTO.getEmail());
        customer1.setCustomerCountry(customerDTO.getCustomerCountry());
        customer1.setCity(customerDTO.getCity());
        customer1.setGender(customerDTO.getGender());

        List<ContactPerson> contactPeoples = new ArrayList<>();
        System.out.println("step 3");

        for (ContactPersonDTO contactPersonDTO : customerDTO.getContactPersonDTOList()){
            ContactPerson contactPerson = new ContactPerson();
            contactPerson.setContactId(contactPersonDTO.getContactId());
            contactPerson.setContactName(contactPersonDTO.getContactName());
            contactPerson.setContactDesignation(contactPersonDTO.getContactDesignation());
            contactPerson.setContactMobile(contactPersonDTO.getContactMobile());
            contactPerson.setContactEmail(contactPersonDTO.getContactEmail());
            contactPerson.setCustomer(customer1);
            contactPeoples.add(contactPerson);
        }
        customer1.setContactPeople(contactPeoples);
        System.out.println("step 4");
        return customer1;

    }
}
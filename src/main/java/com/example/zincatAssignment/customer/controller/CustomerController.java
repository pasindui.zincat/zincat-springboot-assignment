package com.example.zincatAssignment.customer.controller;

import com.example.zincatAssignment.common.entity.Customer;
import com.example.zincatAssignment.common.response.GenericResponse;
import com.example.zincatAssignment.customer.dto.CustomerDTO;
import com.example.zincatAssignment.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping(value = "/{customerName}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public GenericResponse getCustomerDetailByName(@PathVariable("customerName")String customerName){
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setStatus(HttpStatus.OK);
        genericResponse.setResponse(customerService.findCustomerByCustomerName(customerName));
        return genericResponse;
    }

    @PostMapping(value = "/add", produces = {MediaType.APPLICATION_JSON_VALUE})
    public GenericResponse saveCustomer(@RequestBody CustomerDTO customer){
        System.out.println("step 1");
        String savedCustomer = customerService.saveCustomer(customer);
        GenericResponse response = new GenericResponse();
        response.setResponse(HttpStatus.OK);
        response.setResponse(savedCustomer);
        return response;
    }

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public GenericResponse getAllCustomers(){
        GenericResponse genericResponse = new GenericResponse();
        List<CustomerDTO> allCustomers = customerService.getAllCustomers();
        genericResponse.setStatus(HttpStatus.OK);
        genericResponse.setResponse(allCustomers);
        return genericResponse;
    }

    @PutMapping("/customer/{customerId}")
    //@ResponseStatus(HttpStatus.OK)
    public GenericResponse updateCustomer (@PathVariable ("customerId") Integer customerId){
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setStatus(HttpStatus.OK);
        genericResponse.setResponse(customerService.updateCustomer(null));
        return genericResponse;
    }
}
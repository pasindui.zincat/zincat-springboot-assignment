package com.example.zincatAssignment.contactPerson.service.impl;

import com.example.zincatAssignment.common.entity.ContactPerson;
import com.example.zincatAssignment.common.repository.ContactPersonRepository;
import com.example.zincatAssignment.contactPerson.dto.ContactPersonDTO;
import com.example.zincatAssignment.contactPerson.service.ContactPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ContactPersonServiceImpl implements ContactPersonService {

    @Autowired
    ContactPersonRepository contactPersonRepository;

    @Override
    public String saveContactPerson(ContactPersonDTO contactPersonDTO) {
        ContactPerson contactPerson = contactPersonEntityModalMapper(contactPersonDTO);
        contactPersonRepository.save(contactPerson);
        return "contact person saved.!";
    }

    @Override
    public ContactPerson contactPersonEntityModalMapper(ContactPersonDTO contactPersonDTO){
        ContactPerson contactPerson = new ContactPerson();
        contactPerson.setContactId(contactPerson.getContactId());
        contactPerson.setContactName(contactPerson.getContactName());
        contactPerson.setContactDesignation(contactPerson.getContactDesignation());
        contactPerson.setContactMobile(contactPerson.getContactMobile());
        contactPerson.setContactEmail(contactPerson.getContactEmail());
      return contactPerson;
    }

    @Override
    public ContactPersonDTO contactPersonDtoModalMapper(ContactPerson contactPerson){
        ContactPersonDTO contactPersonDTO = new ContactPersonDTO();
        contactPersonDTO.setContactId(contactPersonDTO.getContactId());
        contactPersonDTO.setContactName(contactPersonDTO.getContactName());
        contactPersonDTO.setContactDesignation(contactPersonDTO.getContactDesignation());
        contactPersonDTO.setContactMobile(contactPersonDTO.getContactMobile());
        contactPersonDTO.setContactEmail(contactPersonDTO.getContactEmail());
        return contactPersonDTO;
    }
}
package com.example.zincatAssignment.contactPerson.service;

import com.example.zincatAssignment.common.entity.ContactPerson;
import com.example.zincatAssignment.contactPerson.dto.ContactPersonDTO;
import org.springframework.stereotype.Component;

/**
 * interface for Contact Person
 * @author pasindu
 */

@Component
public interface ContactPersonService {

    String saveContactPerson (ContactPersonDTO contactPersonDTO);

    ContactPerson contactPersonEntityModalMapper(ContactPersonDTO contactPersonDTO);

    ContactPersonDTO contactPersonDtoModalMapper(ContactPerson contactPerson);
}

package com.example.zincatAssignment.contactPerson.controller;

import com.example.zincatAssignment.common.response.GenericResponse;
import com.example.zincatAssignment.contactPerson.dto.ContactPersonDTO;
import com.example.zincatAssignment.contactPerson.service.ContactPersonService;
import com.example.zincatAssignment.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/contactperson")
public class ContactPersonController {

    @Autowired
    ContactPersonService contactPersonService;

    @PostMapping(value = "/add", produces = {MediaType.APPLICATION_JSON_VALUE})
    public GenericResponse saveContactPerson(@RequestBody ContactPersonDTO contactPersonDTO){
        String savedContactPerson = contactPersonService.saveContactPerson(contactPersonDTO);
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setResponse(HttpStatus.OK);
        genericResponse.setResponse(savedContactPerson);
        return genericResponse;


    }
}

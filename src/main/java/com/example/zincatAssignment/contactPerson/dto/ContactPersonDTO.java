package com.example.zincatAssignment.contactPerson.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactPersonDTO {

    private long contactId;

    private String contactName;

    private String contactDesignation;

    private String contactMobile;

    private String contactEmail;
}

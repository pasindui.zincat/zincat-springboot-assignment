package com.example.zincatAssignment.common.repository;

import com.example.zincatAssignment.common.entity.ContactPerson;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactPersonRepository extends JpaRepository <ContactPerson, Long> {

}

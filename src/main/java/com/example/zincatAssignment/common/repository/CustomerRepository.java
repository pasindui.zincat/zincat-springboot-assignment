package com.example.zincatAssignment.common.repository;

import com.example.zincatAssignment.common.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface CustomerRepository extends JpaRepository <Customer, Integer> {

    /**
     * get all customers by name
     *
     * @author pasindu
     */
    Customer getCustomerByCustomerName(String customerName);
}
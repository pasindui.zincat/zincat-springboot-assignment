package com.example.zincatAssignment.common.repository;

import com.example.zincatAssignment.common.entity.Type;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypeRepository extends JpaRepository<Type, Integer> {
}

package com.example.zincatAssignment.common.repository;

import com.example.zincatAssignment.common.entity.File;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FileRepository extends JpaRepository<File, Long> {
}

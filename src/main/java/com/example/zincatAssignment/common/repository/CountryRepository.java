package com.example.zincatAssignment.common.repository;

import com.example.zincatAssignment.common.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Integer> {
}

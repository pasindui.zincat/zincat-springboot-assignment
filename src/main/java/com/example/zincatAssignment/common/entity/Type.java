package com.example.zincatAssignment.common.entity;

import lombok.*;

import javax.persistence.*;

/**
 * customer type class
 * @author pasindu
 */

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name="type")
public class Type {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer typeId;

    @Column (name="typeName")
    private String typeName;
}

package com.example.zincatAssignment.common.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * customer entity class
 * @author pasindu
 */

@Data
@Entity
@Table(name = "customer")
@NoArgsConstructor
@AllArgsConstructor
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Integer customerId;

    @Column(name = "code")
    private int code;

    @Column(name = "refrenceNo")
    private String refrenceNo;

    @Column(name = "customerType")
    private String customerType;

    @Column(name="customerName")
    private String customerName;

    @Column(name = "companyName")
    private String companyName;

    @Column(name = "nic")
    private String nic;

    @Column(name = "billingAddress")
    private String billingAddress;

    @Column(name="customerMobile")
    private String customerMobile;

    @Column(name = "email")
    private String email;

    @Column(name = "customerCountry")
    private String customerCountry;

    @Column(name = "city")
    private String city;

    @Column(name = "gender")
    private String gender;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private List<ContactPerson> contactPeople;

    /**
     * file uploading eka ganath ahanna
     */
}

package com.example.zincatAssignment.common.entity;

import lombok.*;

import javax.persistence.*;

/**
 * contact person entity class
 *
 * @author pasindu
 */

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contactPerson")
public class ContactPerson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long contactId;

    @Column(name = "contactName")
    private String contactName;

    @Column(name = "contactDesignation")
    private String contactDesignation;

    @Column(name = "contactMobile")
    private String contactMobile;

    @Column(name = "contactEmail")
    private String contactEmail;

    @ManyToOne
    @JoinColumn(name = "customerId",referencedColumnName = "customerId")
    private Customer customer;

}

package com.example.zincatAssignment.common.entity;

import lombok.*;

import javax.persistence.*;

/**
 * country entity class
 * @author pasindu
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="country")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer countryId;

    @Column(name="country_name")
    private String countryName;

}

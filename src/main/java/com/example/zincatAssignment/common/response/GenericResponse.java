package com.example.zincatAssignment.common.response;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * class for generic response
 *
 * @author pasindu
 */
@Data
public class GenericResponse {

    private HttpStatus status;

    private Object response;
}

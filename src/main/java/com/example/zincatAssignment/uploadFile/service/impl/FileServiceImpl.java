package com.example.zincatAssignment.uploadFile.service.impl;

import com.example.zincatAssignment.common.entity.File;
import com.example.zincatAssignment.common.repository.FileRepository;
import com.example.zincatAssignment.uploadFile.service.FileService;
import com.example.zincatAssignment.uploadFile.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@Transactional
public class FileServiceImpl implements FileService {

    @Autowired
    FileRepository fileRepository;

    @Override
    public String uploadFile(MultipartFile multipartFile) throws IOException {
        File file = fileRepository.save(File.builder()
                .fileName(multipartFile.getOriginalFilename())
                .fileData(FileUtil.compressImage(multipartFile.getBytes()))
                .build());

        if (file != null){
            return "File uploaded successfully" + multipartFile.getOriginalFilename();
        }
        return null;
    }

}

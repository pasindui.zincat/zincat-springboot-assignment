package com.example.zincatAssignment.uploadFile.controller;

import com.example.zincatAssignment.uploadFile.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping(value = "/file")
public class FileController {

    @Autowired
    FileService fileService;

    @PostMapping("/")
    public ResponseEntity<?> uploadFile(@RequestParam("file")MultipartFile multipartFile) throws IOException{
        String uploadFile = fileService.uploadFile(multipartFile);
        return ResponseEntity.status(HttpStatus.OK).body(uploadFile);
    }

}
